#include "sdcard.h"

#include "sd_cmd.h"

#define RTC_FREQ 32768

//#define SD_DEBUG_PRINT
#ifdef SD_DEBUG_PRINT
#include <metal/time.h>
#include <stdio.h>
#endif

#include <metal/gpio.h>
#include <metal/spi.h>

unsigned char crc7shl1or1(const unsigned char * const buf, const unsigned int len)
{
	const unsigned char poly = 0x89;
	unsigned char crc = 0;
	for (unsigned int i = 0; i < len; i++) {
		crc ^= buf[i];
		for (int j = 0; j < 8; j++) {
			crc = (crc & 0x80U) ? ((crc << 1) ^ (poly << 1)) : (crc << 1);
		}
	}
	return crc | 0x01;
}

/*
 * Sends an SD command and receives response.
 */
int sd_send_command(sd_context_t *sdc, unsigned char cmd, unsigned int response_type, int arg, int data_packets_after)
{
    sdc->tx_buf[0] = 0x40 | cmd;
	sdc->tx_buf[4] = 0xFF & arg; arg >>= 8; // SD spi big endian
	sdc->tx_buf[3] = 0xFF & arg; arg >>= 8;
	sdc->tx_buf[2] = 0xFF & arg; arg >>= 8;
	sdc->tx_buf[1] = 0xFF & arg; arg >>= 8;
	sdc->tx_buf[5] = crc7shl1or1(sdc->tx_buf, 5);

    const unsigned int response_length = SD_RESPONSE_TYPE_LENGTH_LUT[response_type];

    /* send the command */
    sd_delay(sdc, 1);
    SS_ASSERT;
    sd_delay(sdc, 1);
    metal_spi_transfer(spi1, sdc->spi_conf, 6, sdc->tx_buf, sdc->rx_buf);
    #ifdef SD_DEBUG_PRINT
        printf("[T+%08lu] [SPI] [CMD%d]", (unsigned long) metal_time(), (signed int) cmd);
        for (unsigned short i = 0; i < 6; ++i) printf(" %02X", (unsigned char)sdc->tx_buf[i]); printf("\r\n");
    #endif

    /* wait for a response */
    sdc->tx_buf[0] = 0xFF;
    unsigned int i = 0;
    do {
        metal_spi_transfer(spi1, sdc->spi_conf, 1, sdc->tx_buf, sdc->rx_buf);

        ++i;
        if (i >= SD_CMD_TIMEOUT) {
            sd_delay(sdc, 1);
            SS_DEASSERT;
            sd_delay(sdc, 1);
            return 0;
        }
    } while (sdc->rx_buf[0] & 0x80);

    /* handle responses that are longer than 1 byte */
    for (i = 1; i < response_length; ++i) {
        sdc->tx_buf[i] = 0xFF;
    }
    metal_spi_transfer(spi1, sdc->spi_conf, response_length - 1, sdc->tx_buf + 1, sdc->rx_buf + 1);

    #ifdef SD_DEBUG_PRINT
        printf("[T+%08lu] [SPI] [RESP]", (unsigned long) metal_time(), (signed int) cmd);
        for (unsigned short i = 0; i < response_length; ++i) printf(" %02X", (unsigned char)sdc->rx_buf[i]); printf("\r\n");
    #endif

    /* If the response is a "busy" type (R1B), then there's some
     * special handling that needs to be done. The card will
     * output a continuous stream of zeros, so the end of the BUSY
     * state is signaled by any nonzero response. The bus idles
     * high.
     */
    if (response_type == R1b) {
        char tmp;
        #ifdef SD_DEBUG_PRINT
            printf("[T+%08lu] [SPI] [R1b] busy", (unsigned long) metal_time(), (signed int) cmd);
        #endif
        do {
            metal_spi_transfer(spi1, sdc->spi_conf, 1, sdc->tx_buf, &tmp);
            #ifdef SD_DEBUG_PRINT
                printf(".");
            #endif
        } while (!tmp);
        #ifdef SD_DEBUG_PRINT
            printf("\r\n");
        #endif
    }

    // If data packets follow the command, SS should be kept asserted. It is up to the calling
    // function to deassert SS after transferring data.
    if (!data_packets_after) {
		sd_delay(sdc, 1);
		SS_DEASSERT;
		sd_delay(sdc, 1);
    }

    return 1;
}

int sd_set_blocklen(sd_context_t *sdc, uint32_t length)
{
    if (sd_send_command(sdc, SET_BLOCKLEN, R_SET_BLOCKLEN, length, 0) == 1)
        return 1;
#ifdef SD_DEBUG_PRINT
    printf("[T+%08lu] SET_BLOCKLEN 512 DONE\r\n", (unsigned long) metal_time());
#endif

    return 0;
}

/*
 * Receives a packet from the SD card. Read block size does not include the 1 token and 2 CRC bytes.
 * Usually it is 512 bytes, but for the registers it is 16 bytes.
 */
size_t sd_rx_packet(sd_context_t *sdc, const size_t read_block_size, char * const data)
{
#ifdef SD_DEBUG_PRINT
    printf("[T+%08lu] Awaiting data packet rx:", (unsigned long) metal_time());
#endif
	for (size_t i = 0; i < sdc->buf_size; ++i)
		sdc->tx_buf[i] = 0xFF;

	// throw away bytes until start token (timeout after 200 bytes and nothing received)
	size_t bytes_read = 0;
	do {
		metal_spi_transfer(spi1, sdc->spi_conf, 1, sdc->tx_buf, sdc->rx_buf);

#ifdef SD_DEBUG_PRINT
    printf(".");
#endif
		// timeout if the data packet never arrives
		if (++bytes_read == SD_CMD_TIMEOUT) {
#ifdef SD_DEBUG_PRINT
    printf("timeout\r\n");
#endif
			sd_delay(sdc, 2);
			SS_DEASSERT;
			sd_delay(sdc, 2);
			return 0;
		}
	} while (sdc->rx_buf[0] != SD_TOK_READ_STARTBLOCK);
#ifdef SD_DEBUG_PRINT
    printf("!");
#endif

	// start reading ACTUAL data
	metal_spi_transfer(spi1, sdc->spi_conf, read_block_size + 2, sdc->tx_buf, data);
	sd_delay(sdc, 2);
	SS_DEASSERT;
	sd_delay(sdc, 2);

#ifdef SD_DEBUG_PRINT
	for (size_t i = 0; i < read_block_size; ++i) {
		if (i % 32 == 0) printf("\r\n");
		printf(" %02X", (unsigned char)sdc->rx_buf[i]);
	}
	printf("\r\n Checksum: %02X %02X\r\n", (unsigned char)sdc->rx_buf[read_block_size], (unsigned char)sdc->rx_buf[read_block_size + 1]);
#endif

	return bytes_read;
}

int sd_initialize(sd_context_t *sdc)
{
    /* wait 1s */
#ifdef SD_DEBUG_PRINT
    printf("[T+%08lu] Initializing SD card...\r\n", (unsigned long) metal_time());
#endif

	/* wait 1s */
	timer_isr_flag = 0; // clear global timer ISR flag
	metal_cpu_set_mtimecmp(cpu0, metal_cpu_get_mtime(cpu0) + (RTC_FREQ * 1)); // plus a second
	metal_interrupt_enable(tmr_intr, tmr_id); // Enable Timer interrupt
	while (timer_isr_flag == 0); // do nothing while waiting
	timer_isr_flag = 0; // clear global timer ISR flag

    /* start initialization SS high and at least 74 SCK cycles */
    SS_DEASSERT; // pin should still be high from constructor but we have this here just in case
	sd_delay(sdc, 10); // this sends 80 cycles, which is more than enough

    sdc->state = ST_DISCONNECT;

    /* Put the card in the idle state. Early return if unsuccessful. */
    if (sd_send_command(sdc, GO_IDLE_STATE, R_GO_IDLE_STATE, 0, 0) == 0)
        return 1;
    if (sdc->rx_buf[0] != (char)0x01)
        return 1;
#ifdef SD_DEBUG_PRINT
    printf("[T+%08lu] GO_IDLE_STATE success...\r\n", (unsigned long) metal_time());
#endif

    if (sd_send_command(sdc, SEND_IF_COND, R_SEND_IF_COND, 0x1AA, 0) == 0)
        return 1;
    if (((sdc->rx_buf[3] & 0x0F) != (char)0x01) || (sdc->rx_buf[4] != (char)0xAA))
        return 2; // FATAL: SD card incorrect echo response
#ifdef SD_DEBUG_PRINT
    printf("[T+%08lu] SEND_IF_COND success...\r\n", (unsigned long) metal_time());
#endif

    /* CMD55-ACMD41 init loop with 1-second timeout */
    timer_isr_flag = 0; // clear global timer ISR flag
	metal_cpu_set_mtimecmp(cpu0, metal_cpu_get_mtime(cpu0) + (RTC_FREQ * 1)); // plus a second
	metal_interrupt_enable(tmr_intr, tmr_id); // Enable Timer interrupt
    while (1) {
        sd_send_command(sdc, APP_CMD, R_APP_CMD, 0, 0);
        if (sdc->rx_buf[0] == 0x05)
            return 3; // Must be old SD card, try CMD1
        sd_send_command(sdc, SD_SEND_OP_COND, R_SD_SEND_OP_COND, 0x40000000, 0);
        if (sdc->rx_buf[0] == 0x05)
            return 3; // Must be old SD card, try CMD1
        else if (sdc->rx_buf[0] == 0x00)
            break;
        if (timer_isr_flag != 0) {
	        timer_isr_flag = 0; // clear global timer ISR flag
            return 2; // FATAL: SD card did not init within 1 second
        }
    }

    sd_send_command(sdc, READ_OCR, R_READ_OCR, 0, 0);
#ifdef SD_DEBUG_PRINT
    if (sdc->rx_buf[1] & 0x40)
        printf("[T+%08lu] Detected SDHC/XC.\r\n", (unsigned long) metal_time());
    else
        printf("[T+%08lu] Detected SD (regular capacity).\r\n", (unsigned long) metal_time());
#endif

   // SD_BLOCKSIZE
#ifdef SD_DEBUG_PRINT
    printf("[T+%08lu] Set block size to %d.\r\n", (unsigned long) metal_time(), SD_BLOCKSIZE);
#endif
    sd_set_blocklen(sdc, SD_BLOCKSIZE);


#ifdef SD_DEBUG_PRINT
    printf("[T+%08lu] SD card initialization COMPLETE!.\r\n", (unsigned long) metal_time());
#endif
    sdc->state = ST_READY;
    return 0;
}

int sd_get_csd(sd_context_t *sdc)
{
#ifdef SD_DEBUG_PRINT
	printf("[T+%08lu] SEND_CSD\r\n", (unsigned long) metal_time());
#endif
	sd_send_command(sdc, SEND_CSD, R_SEND_CSD, 0, 1);
	sd_rx_packet(sdc, 16, sdc->rx_buf);
	return 0;
}

int sd_get_cid(sd_context_t *sdc)
{
#ifdef SD_DEBUG_PRINT
	printf("[T+%08lu] SEND_CID\r\n", (unsigned long) metal_time());
#endif
	sd_send_command(sdc, SEND_CID, R_SEND_CID, 0, 1);
	sd_rx_packet(sdc, 16, sdc->rx_buf);
	return 0;
}

int sd_read_block(sd_context_t *sdc, const size_t lba, char * const data)
{

	// TODO: if plain old SD, then lba has to be shifted by 9
	// since in the early days they used byte addressing

#ifdef SD_DEBUG_PRINT
	printf("[T+%08lu] READ_SINGLE_BLOCK\r\n", (unsigned long) metal_time());
#endif
	sd_send_command(sdc, READ_SINGLE_BLOCK, R_READ_SINGLE_BLOCK, lba, 1);
	sd_rx_packet(sdc, SD_BLOCKSIZE, data);
#ifdef SD_DEBUG_PRINT
	printf("[T+%08lu] READ_SINGLE_BLOCK: success\r\n", (unsigned long) metal_time(), (unsigned int) sdc->rx_buf[1]);
#endif
}

int sd_write_block(sd_context_t *sdc, const size_t lba, char * const data)
{
	sd_send_command(sdc, WRITE_BLOCK, R_WRITE_BLOCK, lba, 1);

	sdc->tx_buf[0] = 0xFE; // Start of data flag
	metal_spi_transfer(spi1, sdc->spi_conf, 1, sdc->tx_buf, sdc->rx_buf);

	metal_spi_transfer(spi1, sdc->spi_conf, 512, data, sdc->rx_buf);

	// dummy crc bytes
	metal_spi_transfer(spi1, sdc->spi_conf, 2, sdc->tx_buf, sdc->rx_buf);

	// wait till busy is done (timeout after 200 bytes and nothing received)
	size_t bytes_read = 0;
	do {
		metal_spi_transfer(spi1, sdc->spi_conf, 1, sdc->tx_buf, sdc->rx_buf);

#ifdef SD_DEBUG_PRINT
		printf(".");
#endif
		// timeout if the data packet never arrives
		if (++bytes_read == SD_CMD_TIMEOUT) {
#ifdef SD_DEBUG_PRINT
			printf("timeout\r\n");
#endif
			break;
		}
	} while (sdc->rx_buf[0] & 0x08);

	sd_delay(sdc, 2);
	SS_DEASSERT;
	sd_delay(sdc, 2);
}

void sd_delay(sd_context_t *sdc, const unsigned int number)
{
    char tmp_tx = 0xFF, tmp_rx;
    for (unsigned int i = 0; i < number; ++i)
        metal_spi_transfer(spi1, sdc->spi_conf, 1, &tmp_tx, &tmp_rx);
}
