#pragma once

const char* const test_nmea_nofix =
		"$GPRMC,200000.00,V,,,,,,,240422,,,N\r\n"
		"$GPVTG,,,,,,,,,N*30\r\n"
		"$GPGGA,200000.00,,,,,0,00,99.99,,,,,,*64\r\n"
		"$GPGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99\r\n"
		"$GPGSV,4,1,16,02,49,262,19,03,05,036,,05,01,199,11,06,68,000,28*7E\r\n"
		"$GPGSV,4,2,16,09,00,101,,11,52,251,,12,28,319,,14,25,144,25*76\r\n"
		"$GPGSV,4,3,16,17,40,065,,19,51,041,30,20,28,190,23,24,20,269,*7A\r\n"
		"$GPGSV,4,4,16,28,53,143,,46,34,241,,48,37,237,,51,50,216,*7C\r\n"
		"$GPGLL,,,,,200000.00,V,N*48\r\n";
