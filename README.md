# FE310 SD Card
FAT32 on SD card in SPI mode.

| Pin | Description | GPIO | Comment |
| --- | --- | --- | --- |
| 10 | `SS` | 2 | Software control |
| 11 | `MOSI` | 3 | |
| 12 | `MISO` | 4 | FE310 internal pull-up enabled |
| 13 | `SCK` | 5 | 400 kHz |
