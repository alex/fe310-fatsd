#ifndef SDCARD_H
#define SDCARD_H

#include "global_handle.h"

typedef enum {
	ST_DISCONNECT = 0,       /* 0: No SD card inserted */
	ST_IDLE,                 /* 1: Card detected via CMD0 response */
	ST_READY,                /* 2: Initialized */
	ST_BUSY,                 /* 3: Write operation in progress */
	ST_ERR,                  /* 4: Error state, need to reset SD card */
} sd_state_t;

typedef struct sd_context {
	sd_state_t state;

    char *tx_buf;
    char *rx_buf;
    unsigned int buf_size;

    struct metal_spi_config *spi_conf;
    unsigned int timeout_read;
    unsigned int timeout_write;
} sd_context_t;

#define SD_BLOCKSIZE 512
#define SD_BLOCKSIZE_NBITS 9

#define SS_ASSERT	metal_gpio_set_pin(gpio0, BOARD_PIN_10_SS, 0);
#define SS_DEASSERT	metal_gpio_set_pin(gpio0, BOARD_PIN_10_SS, 1);

/* Internal functions, used for SD card communications. */
int sd_set_blocklen(sd_context_t *sdc, uint32_t length);
void sd_delay(sd_context_t *sdc, const unsigned int number);

/* User functions */
int sd_initialize(sd_context_t *sdc);
int sd_read_block(sd_context_t *sdc, const size_t lba, char * const data);
int sd_write_block(sd_context_t *sdc, const size_t lba, char * const data);
void sd_wait_notbusy(sd_context_t *sdc);

int sd_get_csd(sd_context_t *sdc);
int sd_get_cid(sd_context_t *sdc);

#endif
