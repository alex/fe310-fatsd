/* Copyright 2019 SiFive, Inc */
/* SPDX-License-Identifier: Apache-2.0 */

#include "global_handle.h"
#include "sdcard.h"

#include <metal/clock.h>
#include <metal/cpu.h>
#include <metal/gpio.h> //include GPIO library, https://sifive.github.io/freedom-metal-docs/apiref/gpio.html
#include <metal/init.h>
#include <metal/machine.h>
#include <metal/spi.h>
#include <metal/time.h>       //include Time library

#include <stdio.h>      //include Serial Library
#include <string.h> // needed for memset

struct metal_spi_config *conf;

#define SPI_BUF_SIZE 540
unsigned char spi_tx_buf[SPI_BUF_SIZE];
unsigned char spi_rx_buf[SPI_BUF_SIZE];

unsigned char text_buf[512];

struct metal_cpu *cpu;
struct metal_interrupt *cpu_intr;
struct metal_interrupt *uart1_ic;
int uart1_irq;
struct metal_uart *uart1;

#include "nmea.h"

void delay_1s(void) {
    metal_cpu_set_mtimecmp(cpu0, metal_cpu_get_mtime(cpu0) + 32768);
    metal_interrupt_enable(tmr_intr, tmr_id);

    while (!timer_isr_flag)
    	__asm__ volatile("wfi");

    putchar('.');

    timer_isr_flag = 0;
}

void uart1_puts(const char *__restrict s) {
	for (int i = 0; s[i]; ++i) {
		//metal_uart_putc(uart1, s[i]);
	}
}

void uart1_isr (int id, void *data) {
    metal_uart_receive_interrupt_disable(uart1);

    //putchar('>');

	int temp;
	while (1) {
		temp = (*((volatile uint32_t *) (0x10023000+4)));
		if (0x80000000 & temp)
			break;
		else
			//putchar(0x000000FF & temp);
			process_char((char)(0x000000FF & temp));
	}
    //putchar('\n');

    metal_uart_receive_interrupt_enable(uart1);
}

int main (void)
{
	printf("partial");
	printf(" line");
	printf(" test\r\n");


	printf("[T+%08lu] *** RED-V SD over SPI Test ***\r\n", (unsigned long) metal_time());

	fputs("starting...", stdout);
	delay_1s();
	putchar('.');
	delay_1s();
	puts(".");

	// https://github.com/westerndigitalcorporation/RISC-V-Linux/blob/master/linux/Documentation/spi/spi-summary
	// CPOL = 0, CPHA = 0, MSB-first, CS active low
	struct metal_spi_config sd_conf =
	{
		.protocol = METAL_SPI_SINGLE,
		.polarity = 0,
		.phase = 0,
		.cs_active_high = 0,
		.csid = 0,
		.cmd_num = 0,
		.addr_num = 0,
		.dummy_num = 0
	};

	struct sd_context sdc =
	{
		.spi_conf = &sd_conf,
		.timeout_read = 400000 / 20, // 400kHz rate -> 400ms timeout
		.timeout_write = 400000 / 8, // 400kHz rate -> 1s timeout
		.state = ST_DISCONNECT,
		.tx_buf = spi_tx_buf,
		.rx_buf = spi_rx_buf,
		.buf_size = SPI_BUF_SIZE     // should be large enough to hold a block plus some extra
	};

	memset(sdc.tx_buf, 0xFF, sdc.buf_size);
	memset(sdc.rx_buf, 0xFF, sdc.buf_size);


	char nmea_message_buf[NMEA_MESSAGE_MAX];
	nmea.state = ST_INIT,
	nmea.message = nmea_message_buf;
	nmea.message_len = 0;
	nmea.message_max = NMEA_MESSAGE_MAX;

    // Lets get the CPU and and its interrupt
    cpu = metal_cpu_get(metal_cpu_get_current_hartid());
    cpu_intr = metal_cpu_interrupt_controller(cpu);
    metal_interrupt_init(cpu_intr);

    // Setup UART 1 and its interrupt
    uart1 = metal_uart_get_device(1);
    metal_uart_init(uart1, 9600);
    uart1_ic = metal_uart_interrupt_controller(uart1);
    metal_interrupt_init(uart1_ic);
    uart1_irq = metal_uart_get_interrupt_id(uart1);
    metal_interrupt_register_handler(uart1_ic, uart1_irq, uart1_isr, NULL);

    // Lets enable the Uart interrupt
    metal_uart_set_receive_watermark(uart1, 3);
    metal_uart_receive_interrupt_enable(uart1);
    metal_interrupt_enable(uart1_ic, uart1_irq);

    // Lastly CPU interrupt
    metal_interrupt_enable(cpu_intr, 0);

    __asm__ volatile("wfi");


    ///////////////////////////////////////////////


    sd_initialize(&sdc);

    	if (sdc.state != ST_READY) {
    		printf("Error initializing sd card\r\n    skipping...\r\n");
    		while(1) {

            	delay_1s();
            	putchar('#');
            	metal_uart_putc(uart1, '^');
    		}
    	}

    	sd_get_csd(&sdc);
    	sd_get_cid(&sdc);

    	printf("[T+%08lu] [CID] ", (unsigned long) metal_time());
    	printf("%c", sdc.rx_buf[3]);
    	printf("%c", sdc.rx_buf[4]);
    	printf("%c", sdc.rx_buf[5]);
    	printf("%c", sdc.rx_buf[6]);
    	printf("%c", sdc.rx_buf[7]);
    	printf(" rev. %d.%d    ", sdc.rx_buf[8] >> 4, sdc.rx_buf[8] & 0x0F);
    	printf("Serial %04X:%04X\r\n", sdc.rx_buf[9] << 8 | sdc.rx_buf[10], sdc.rx_buf[11] << 8 | sdc.rx_buf[12]);

    	printf("consecutive block read test (512-byte blocks)\r\n\r\n");
    	metal_spi_set_baud_rate(spi1, 1500000);
    	for (size_t d = 0; d < 4/*2097152U*/; ++d) {
    		struct metal_uart *uart0 = metal_uart_get_device(0);
    		metal_uart_putc(uart0, '^');
    		printf("Reading sector %u:\r\n", d);
    		sd_read_block(&sdc, d, sdc.rx_buf);

    		for (unsigned ptr = 0; ptr < 512; ++ptr)
    		{
    			unsigned char current = sdc.rx_buf[ptr];

    			if ((ptr % 16) == 15) {
    				printf("%02X\r\n", current);
    			} else {
    				printf("%02X ", current);
    			}
    		}
    	}



        while (1) {
            //
        	//delay_1s();
        	putchar('\n');
        }
    return 0;
}
